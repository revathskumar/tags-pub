// post-tag-inbox-test.js -- POST /tag/:tag/inbox
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')

const env = require('./env')
const inboxBatch = require('./inbox-batch')

const AS2 = 'https://www.w3.org/ns/activitystreams'
const AS2_MIME = 'application/activity+json'
const TAG = 'foo'

const id = `${env.TAGS_PUB_URL_ROOT}/tag/${TAG}`
const activity = {
  '@context': AS2,
  'id': 'http://localhost:8080/activity/follow/1',
  'type': 'Follow',
  'summary': `Evan follows #foo`,
  'cc': ['https://www.w3.org/ns/activitystreams#Public'],
  'to': [id],
  'actor': {
    'type': 'Person',
    'id': 'http://localhost:8080/person/evan',
    'name': 'Evan Prodromou'
  },
  'object': {
    'type': 'Hashtag',
    'id': id,
    'name': `#${TAG}`
  }
}

vows.describe('POST Follow activity to /tag/:tag/inbox')
  .addBatch(inboxBatch(activity, {
    'and we get the followers collection': {
      async topic () {
        const root = `http://${env.TAGS_PUB_HOSTNAME}:${env.TAGS_PUB_PORT}`
        const url = `${root}/tag/${TAG}/followers`
        return fetch(url, {'headers': {'Accept': AS2_MIME}})
      },
      'it works': (err, res) => {
        assert.ifError(err)
        assert.isObject(res)
      },
      'and we examine the response body': {
        async topic (res) {
          return res.json()
        },
        'it works': (err, json) => {
          assert.ifError(err)
          assert.isObject(json)
        },
        'it includes the person who sent the Follow activity': (err, json) => {
          assert.ifError(err)
          assert.isObject(json)
          assert.isObject(json.first)
          assert.isArray(json.first.items)
          assert.lengthOf(json.first.items, 1)
          assert.equal(json.first.items[0], 'http://localhost:8080/person/evan')
        }
      }
    }
  }))
  .export(module)
