// tag-key-test.js -- Test the /tag/:tag/key endpoint
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const vows = require('perjury')
const {assert} = vows
const fetch = require('node-fetch')
const debug = require('debug')('tags-pub:tag-key-test')

const Server = require('./server')
const env = require('./env')

const AS2 = 'https://www.w3.org/ns/activitystreams'
const SEC = 'https://w3id.org/security/v1'

const ACCEPT = 'application/activity+json;1.0,application/ld+json;0.5,application/json;0.1'

const AS2_MIME = 'application/activity+json'
const JSONLD_MIME = 'application/ld+json'
const JSONLD_MIME_FULL = 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
const JSON_MIME = 'application/json'
const JSON_MIME_FULL = 'application/json; charset=utf-8'

function fetchTagKey (tag, accept = ACCEPT, expected = AS2_MIME) {
  const base = {
    topic () {
      const root = `http://${env.TAGS_PUB_HOSTNAME}:${env.TAGS_PUB_PORT}`
      const url = `${root}/tag/${tag}/key`
      return fetch(url, {'headers': {'Accept': accept}})
    },
    'it works': (err, res) => {
      assert.ifError(err)
      assert.isObject(res)
      assert.equal(res.status, 200)
    },
    'it has the right MIME type': (err, res) => {
      assert.ifError(err)
      assert.isObject(res)
      assert.equal(res.status, 200)
      assert.ok(res.headers.has('Content-Type'))
      assert.equal(res.headers.get('Content-Type'), expected)
    },
    'and we get its JSON contents': {
      topic (res) {
        return res.json()
      },
      'it works': (err, json) => {
        assert.ifError(err)
        debug(json)
        assert.isObject(json)
        assert.isArray(json['@context'], 'Context is not an array')
        assert.lengthOf(json['@context'], 2)
        assert.isString(json['@context'][0])
        assert.equal(json['@context'][0], AS2)
        assert.isString(json['@context'][1])
        assert.equal(json['@context'][1], SEC)
        assert.include(json, 'id')
        assert.isString(json.id)
        assert.include(json, 'type')
        assert.isString(json.type)
        assert.equal(json.type, 'Key')
        assert.include(json, 'owner')
        assert.isString(json.owner)
        assert.include(json, 'publicKeyPem')
        assert.isString(json.publicKeyPem)
      }
    }
  }
  return base
}

vows.describe('/tag/:tag/key endpoint')
  .addBatch(Server.batch(env, {
    'and we fetch a tag public key': {
      topic () {
        return true
      },
      'with default accepts':
        fetchTagKey('foo', ACCEPT, AS2_MIME),
      'with application/ld+json':
        fetchTagKey('foo', JSONLD_MIME, JSONLD_MIME_FULL),
      'with application/json':
        fetchTagKey('foo', JSON_MIME, JSON_MIME_FULL)
    }
  }))
  .export(module)
