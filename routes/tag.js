// index.js -- main routes for tags.pub
//
// Copyright 2017 Evan Prodromou <legal@imfn.me>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const express = require('express')
const {parseRequest, verifySignature} = require('http-signature')
const debug = require('debug')('tags-pub:route-tags')
const fetch = require('node-fetch')
const {queue} = require('async')
const _ = require('lodash')
const uuidv1 = require('uuid/v1')

const router = express.Router()

const AS2 = 'https://www.w3.org/ns/activitystreams'
const SEC = 'https://w3id.org/security/v1'

const PUBLIC = 'https://www.w3.org/ns/activitystreams#Public'

const ensureTagPublicKey = async (req, res, next) => {
  const {db} = req.app
  const {tag} = req.params
  try {
    const publicKeyPem = await db.read('public-key', tag)
    req.publicKeyPem = publicKeyPem
    next()
  } catch (err) {
    if (err.name === 'NoSuchThingError') {
      const pair = await req.app.generateKeypair(tag)
      await Promise.all([
        db.save('public-key', tag, pair.public),
        db.save('private-key', tag, pair.private)
      ])
      req.publicKeyPem = pair.public
      next()
    } else {
      next(err)
    }
  }
}

/* GET tag */

router.get('/:tag', ensureTagPublicKey, (req, res, next) => {
  const {tag} = req.params
  const obj = {
    '@context': [AS2, SEC],
    type: 'Hashtag',
    id: req.app.makeURL(`/tag/${tag}`),
    name: `#${tag}`,
    publicKey: {
      id: req.app.makeURL(`/tag/${tag}/key`),
      type: 'Key',
      owner: req.app.makeURL(`/tag/${tag}`),
      publicKeyPem: req.publicKeyPem
    }
  }
  const props = ['inbox', 'outbox', 'followers', 'following', 'liked']
  for (const prop of props) {
    obj[prop] = req.app.makeURL(`/tag/${tag}/${prop}`)
  }
  if (req.accepts('text/html')) {
    res.render('tag', {tag: obj, title: obj.name})
  } else {
    res.as2(obj)
  }
})

/* GET tag */

router.get('/:tag/key', ensureTagPublicKey, (req, res, next) => {
  const {tag} = req.params
  const obj = {
    '@context': [AS2, SEC],
    id: req.app.makeURL(`/tag/${tag}/key`),
    type: 'Key',
    owner: req.app.makeURL(`/tag/${tag}`),
    publicKeyPem: req.publicKeyPem
  }
  res.as2(obj)
})

/* GET tag liked */

router.get('/:tag/liked', (req, res, next) => {
  const {tag} = req.params
  const obj = {
    '@context': AS2,
    type: 'Collection',
    id: req.app.makeURL(`/tag/${tag}/liked`),
    summary: `objects and activities liked by #${tag}`,
    attributedTo: req.app.makeURL(`/tag/${tag}`),
    totalItems: 0
  }
  res.as2(obj)
})

/* GET tag following */

router.get('/:tag/following', (req, res, next) => {
  const {tag} = req.params
  const obj = {
    '@context': AS2,
    type: 'Collection',
    id: req.app.makeURL(`/tag/${tag}/following`),
    summary: `objects that #${tag} is following`,
    attributedTo: req.app.makeURL(`/tag/${tag}`),
    totalItems: 0
  }
  res.as2(obj)
})

/* GET tag inbox */

router.get('/:tag/inbox', (req, res, next) => {
  res.sendStatus(403)
})

const getPublicKey = async (keyId) => {
  debug(`fetching ${keyId}`)
  const res = await fetch(keyId)
  debug(`got response code ${res.status}`)
  debug(`got response headers ${res.headers}`)
  const json = await res.json()
  debug(json)
  if (!json.publicKey || !json.publicKey.publicKeyPem) {
    throw new Error(`No public key found for ${keyId}`)
  }
  return json.publicKey.publicKeyPem
}

const matchId = (obj, tagId) => {
  return ((_.isObject(obj) && obj.id === tagId) || (_.isString(obj) && obj === tagId))
}

const isTagged = (obj, tagId) => {
  return _.isArray(obj.tag) ? _.some(obj.tag, (item) => { return matchId(item, tagId) }) : matchId(obj.tag, tagId)
}

const isAddressedTo = (obj, address) => {
  return _.some(['to', 'cc'], (prop) => {
    return _.isArray(obj[prop]) ? _.some(obj[prop], (item) => { return matchId(item, address) }) : matchId(obj[prop], address)
  })
}

const inboxHandler = async (task) => {
  const {makeURL, db, tag, tagId, keyId, activity} = task
  if (keyId !== activity.actor.id) {
    throw new Error(`key id ${keyId} does not match activity actor id ${activity.actor.id}`)
  }
  switch (activity.type) {
    case 'Follow': {
      debug('Got Follow activity')
      if (activity.object.id !== tagId) {
        throw new Error(`Got a Follow activity for another object: ${activity.object.id} !== ${tagId}`)
      }
      debug('checking if it is already a follower')
      let i = null
      try {
        i = await db.indexOf('followers', tag, activity.actor.id)
      } catch (err) {
        if (err.name === 'NoSuchThingError') {
          i = -1
        } else {
          throw err
        }
      }
      debug(`Got result ${i}`)
      if (i !== -1) {
        throw new Error(`actor ${activity.actor.id} already follows tag ${tag}`)
      }
      debug(`Adding ${activity.actor.id} to list of followers for ${tag}`)
      return db.append('followers', tag, activity.actor.id)
    }
    default: {
      if ((isTagged(activity, tagId) || isTagged(activity.object, tagId)) && isAddressedTo(activity, PUBLIC)) {
        const uuid = uuidv1()
        const announce = {
          'id': makeURL(`/tag/${tag}/announce/${uuid}`),
          '@context': AS2,
          type: 'Announce',
          actor: {
            id: tagId,
            type: 'Hashtag',
            name: `#${tag}`
          },
          object: activity
        }
        return Promise.all([
          db.append('outbox', tag, announce),
          db.save('activity', uuid, announce)
        ])
      }
    }
  }
}

const inboxQueue = queue(inboxHandler)

router.post('/:tag/inbox', async (req, res, next) => {
  debug('parsing request')
  const parsed = parseRequest(req)
  debug(parsed)
  debug(`getting public key for ${parsed.params.keyId}`)
  const pub = await getPublicKey(parsed.params.keyId)
  debug(`verifying signature for ${parsed.params.keyId}`)
  if (verifySignature(parsed, pub)) {
    const task = {
      db: req.app.db,
      tag: req.params.tag,
      keyId: parsed.params.keyId,
      activity: req.body,
      tagId: req.app.makeURL(`/tag/${req.params.tag}`),
      makeURL: (rel) => { return req.app.makeURL(rel) }
    }
    inboxQueue.push(task, (err) => {
      debug('Finished queued task')
      debug(`Got error ${err}`)
    })
    res.sendStatus(202)
  } else {
    res.sendStatus(403)
  }
})

router.post('/:tag/outbox', (req, res, next) => {
  res.sendStatus(403)
})

const PAGE_SIZE = 10

async function tagOutboxPageItems (db, tag, page) {
  const begin = (page - 1) * PAGE_SIZE
  const end = begin + PAGE_SIZE
  try {
    const slice = await db.slice('outbox', tag, begin, end)
    return slice
  } catch (err) {
    if (err.name === 'NoSuchThingError') {
      return []
    } else {
      throw err
    }
  }
}

async function tagOutboxPage (req, db, tag, page) {
  const items = await tagOutboxPageItems(db, tag, page)
  const obj = {
    '@context': AS2,
    type: 'OrderedCollectionPage',
    id: req.app.makeURL(`/tag/${tag}/outbox/page/${page}`),
    summary: `page ${page} of activities by #${tag}`,
    items: items
  }
  return obj
}

async function tagOutboxLength (db, tag) {
  try {
    const count = await db.length('outbox', tag)
    return count
  } catch (err) {
    if (err.name === 'NoSuchThingError') {
      return 0
    } else {
      throw err
    }
  }
}

router.get('/:tag/outbox', async (req, res, next) => {
  const {tag} = req.params
  const {db} = req.app
  const count = await tagOutboxLength(db, tag)
  const page = await tagOutboxPage(req, db, tag, 1)
  const obj = {
    '@context': AS2,
    type: 'OrderedCollection',
    id: req.app.makeURL(`/tag/${tag}/outbox`),
    summary: `activities by #${tag}`,
    totalItems: count,
    attributedTo: req.app.makeURL(`/tag/${tag}`),
    first: page
  }
  res.as2(obj)
})

router.get('/:tag/outbox/page/:n', async (req, res, next) => {
  const {tag, n} = req.params
  const obj = await tagOutboxPage(req, req.app.db, tag, n)
  res.as2(obj)
})

async function tagFollowersPageItems (db, tag, page) {
  const begin = (page - 1) * PAGE_SIZE
  const end = begin + PAGE_SIZE
  try {
    const slice = await db.slice('followers', tag, begin, end)
    return slice
  } catch (err) {
    if (err.name === 'NoSuchThingError') {
      return []
    } else {
      throw err
    }
  }
}

async function tagFollowersPage (req, db, tag, page) {
  const items = await tagFollowersPageItems(db, tag, page)
  const obj = {
    '@context': AS2,
    type: 'OrderedCollectionPage',
    id: req.app.makeURL(`/tag/${tag}/followers/page/${page}`),
    summary: `page ${page} of followers of #${tag}`,
    items: items
  }
  return obj
}

async function tagFollowersLength (db, tag) {
  try {
    const count = await db.length('followers', tag)
    return count
  } catch (err) {
    if (err.name === 'NoSuchThingError') {
      return 0
    } else {
      throw err
    }
  }
}

router.get('/:tag/followers', async (req, res, next) => {
  const {tag} = req.params
  const {db} = req.app
  const count = await tagFollowersLength(db, tag)
  const page = await tagFollowersPage(req, db, tag, 1)
  const obj = {
    '@context': AS2,
    type: 'OrderedCollection',
    id: req.app.makeURL(`/tag/${tag}/followers`),
    summary: `followers of #${tag}`,
    totalItems: count,
    attributedTo: req.app.makeURL(`/tag/${tag}`),
    first: page
  }
  res.as2(obj)
})

router.get('/:tag/followers/page/:n', async (req, res, next) => {
  const {tag, n} = req.params
  const obj = await tagFollowersPage(req, req.app.db, tag, n)
  res.as2(obj)
})

router.get('/:tag/announce/:uuid', async (req, res, next) => {
  const {db} = req.app
  const {uuid} = req.params
  const announce = await db.read('activity', uuid)
  res.as2(announce)
})

module.exports = router
